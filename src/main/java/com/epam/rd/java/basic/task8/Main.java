package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

		DocumentBuilder documentBuilder = dbf.newDocumentBuilder();

		// PLACE YOUR CODE HERE

		// sort (case 1)
		// PLACE YOUR CODE HERE
		
		// save
		String outputXmlFile = "output.dom.xml";
		// PLACE YOUR CODE HERE

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		
		// sort  (case 2)
		// PLACE YOUR CODE HERE
		
		// save
		outputXmlFile = "output.sax.xml";
		// PLACE YOUR CODE HERE
		
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		
		// sort  (case 3)
		// PLACE YOUR CODE HERE
		
		// save
		outputXmlFile = "output.stax.xml";
		// PLACE YOUR CODE HERE
	}
	/*DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
	* builderFacotory.setNamespaceAware(true);
	* DocumentBuilder documentBuilder = builderFactory.newDocumentBuilder();
	* doc = documentBuilde.newDocument();
	* Element root = document.createElement("flowers);
	* root.setAttribute("xmlns","http://www.nure.ua");
	* root.setAttirbute("xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance");
	* root.setAttribute("xsi:schemaLocation","http://www.nure.ua input.xsd");
	* doc.appendChild(root);
	* for(Flower f : flowerList{
	* Elemnt flower = doc.createElement("flower");
	* element = doc.createElement("name");
	* element.setTextContent("f.getName());
	* flower.appendChild(elemnt);
	* element.setTextContent(f.getSoild());
	* flower.appendChild(element);
	* element = document.createElemnt("origin");
	* element.setTextContent(f.getOrigin());
	* flower.appendChild(element);
	* StreamResult1 result1 = new StreamResult (new File("output.dom.xml"));
	* StreamResult1 result2 = new StreamResult (new File("output.sax.xml"));
	* StreamResult1 result3 = new StreamResult (new File("output.stax.xml"));
	* TransformerFactory tf = TransformerFactory.newInstance();
	* javax.xml.transform.Transformer t = tf.newTransformer();
	* t.setOutputProperty(OutputKeys.INDENT, "yes");
	* t.transform(new DOMSource(doc), result1);
	* t.transform(new DOMSource(doc), result2);
	* t.transform(new DOMSource(doc), result3);
	*
	*
	*
	* */
}
